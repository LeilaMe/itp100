from gasp import *
count = 0
correct = 0
while count <= 9:
    num1 = random_between(3, 15)
    num2 = random_between(1, 10)
    question = "What is " + str(num1) + " times " + str(num2) + "? "
    answer = read_number(question)
    realanswer = int(num1) * int(num2)
    if int(answer) == realanswer:
        correct += 1
        print("That's right! Good job")
    else:
        print("no I'm afraid the answer is " + str(realanswer))
    count = count + 1
print("I asked you " + str(count) + " questions. You got " + str(correct) + " of them right.")
if correct <= 6:
    print("try harder next time")
elif correct > 6 and correct < 9:
    print("nice job")
elif correct >=9:
    print("Good job! A plus")
else:
    print("idk")

