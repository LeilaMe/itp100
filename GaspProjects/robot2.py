from gasp import *

class Player:
    pass
class Robot:
    pass

begin_graphics()
finished = False

def place_player():
    global player
    player = Player()
    print("Here I am!")

def collided(player, robots):
    global robot
    for robot in robots:
        if robot.x == player.x and robot.y == player.y:
            return True
    return False

def safely_place_player():
    global player
    place_player()
    player.x = random_between(0, 62)
    player.y = random_between(1, 47)
    player.shape = Circle((10*player.x+5, 10*player.y+5), 5, filled=True)

    while collided(player, robots):
        place_player()
        player.shape = Circle((10*player.x+5, 10*player.y+5), 5, filled=True)

def move_player():
    print("I'm moving...")
    global player
    key = update_when('key_pressed')
    while key == '5':
        remove_from_screen(player.shape)
        safely_place_player()
        key = update_when('key_pressed')
# teleportation device
    if key == '9' and player.x < 62:
        player.x += 1
# moves right(east) probably
    elif key == '1' and player.x > 0:
        player.x -= 1
# hopefully left
    elif key == '2':
        if player.x > 0:
            player.x -= 1
        if player.y > 1:
            player.y -=1
#down and left
    elif key == '3':
        if player.x > 0:
            player.x -= 1
        if player.y < 47:
            player.y += 1
#up and left
    elif key == '4' and player.y > 1:
        player.y -= 1
#down
    elif key == '6':
        if player.x < 62:
            player.x += 1
        if player.y > 1:
            player.y -= 1
# down right aaa
    elif key == '7' and player.y < 47:
        player.y += 1
# STRAIGHT UP oooo
    elif key == '8':
        if player.x < 62:
            player.x += 1
        if player.y < 47:
            player.y += 1
#UP RIGHT eeieieiei
    move_to(player.shape, (10*player.x+5, 10*player.y+5))

numbots = random_between(3, 10)

def place_robots():
    global robots
    robots = []
    while len(robots) < numbots:
        robot = Robot()
        robot.x = random_between(0, 62)
        robot.y = random_between(1, 47)
        if not collided(robot, robots):
            robot.shape = Box((10*robot.x, 10*robot.y), 8, 8)
            robots.append(robot)
def move_robots():
    global robots
    key = update_when('key_pressed')
    for robot in robots:
        if robot.x < player.x:
            if robot.y < player.y:
                robot.x += 1
                robot.y += 1
            elif robot.y > player.y:
                robot.x += 1
                robot.y -= 1
            else:
                robot.x += 1
# to the right
        elif robot.x == player.x:
            if robot.y < player.y:
                robot.y += 1
            elif robot.y > player.y:
                robot.y -= 1
# up or down
        elif robot.x > player.x:
            if robot.y < player.y:
                robot.x -= 1
                robot.y += 1
            elif robot.y > player.y:
                robot.x -= 1
                robot.y -= 1
            else:
                robot.x -= 1
# from the right
        move_to(robot.shape, (10*robot.x, 10*robot.y))

junk = []
def check_collisions():
    global robots, junk, finished, surviving_robots
    surviving_robots = []
    for a in robots:
        if collided(a, junk):
            continue
        robot_crashed(a)
        jbot = robot_crashed(a)
        if jbot == False:
            surviving_robots.append(a)
        else:
            remove_from_screen(jbot.shape)
            jbot.shape = Box((10*jbot.x, 10*jbot.y), 10, 10, filled=True)
            junk.append(jbot)
    robots = []
    for i in surviving_robots:
        if collided(i, junk):
            continue
        else:
            robots.append(i)
    if not robots:
        finished = True
        Text("You win!", (120, 240), size=36)
        sleep(3)
        return
    if collided(player, robots+junk):
        finished = True
        print('oof')
        words = "You've been caught!"
        message = Text(words, (320, 240), color=color.BLACK, size=30)
        sleep(3)
        return

def robot_crashed(thebot):
    global robots
    for abot in robots:
        if abot == thebot:
            return False
        if abot.x == thebot.x and abot.y == thebot.y:
            return abot
    return False

#def score():
#    global junk
 #   Box((630, 0), 10, 10, filled=True, color=color.BLACK)
  #  apples = Text(str(len(junk)), (635, 0), color=color.WHITE, size=10)

Text("1:left 2:down,left 3:up,left 4:down 5:teleport 6:down,right 7:up 8:up,right 9:right", (120, 5), color=color.BLACK, size=10)

place_robots()
safely_place_player()

while not finished:
    move_player()
    move_robots()
    check_collisions()
#    score()

end_graphics()
