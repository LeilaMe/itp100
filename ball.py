from gasp import *
begin_graphics()
ball = Circle((4, 4), 4)
ball_x = 4
ball_y = 4
while ball_x < 636:
    ball_x = ball_x + 4
    ball_y = ball_y + 3
    move_to(ball, (ball_x, ball_y))
    sleep(0.02)
remove_from_screen(ball)

update_when('key_pressed')
end_graphics()
