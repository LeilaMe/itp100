def average(nums):
    """
    Return the average of a list of numbers.
        
        >>> average([2,4])
        3.0
        >>> average([2,6])
        4.0
        >>> average([1, 2, 2, 1, 2, 1])
        1.5
    """
    return sum(nums) / len(nums)
if __name__ == '__main__':
    import doctest
    doctest.testmod()
