def gcf(m, n):
    """
    >>> gcf(10, 25)
    5
    >>> gcf(8, 12)
    4
    >>> gcf(5, 12)
    1
    >>> gcf(24, 12)
    12
    """
    if m > n: 
        count = n
        while count >= 1:
            if m % count == 0 and n % count == 0:
                return count
            count -= 1
    if m < n:
        count = m
        while count >= 1:
            if m % count == 0 and n % count == 0:
                return count
            count -= 1
    else:
        return m

if __name__ == '__main__':
    import doctest
    doctest.testmod()
