def gcf(m, n):
    if m > n: 
        count = n
        while count >= 1:
            if m % count == 0 and n % count == 0:
                return count
            count -= 1
    if m < n:
        count = m
        while count >= 1:
            if m % count == 0 and n % count == 0:
                return count
            count -= 1
    else:
        return m
a = int(input('please enter a number: '))
b = int(input("please enter another number: "))
print(gcf(a, b))
