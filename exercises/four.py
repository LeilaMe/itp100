def lots_of_letters(word):
    """
    >>> lots_of_letters('Lidia')
    'Liidddiiiiaaaaa'
    >>> lots_of_letters('Python')
    'Pyyttthhhhooooonnnnnn'
    >>> lots_of_letters('')
    ''
    >>> lots_of_letters('1')
    '1'
    """
    new = ''
    a = 1
    for i in str(word):
        new = new + (a * i)
        a += 1
    return new

if __name__ == '__main__':
    import doctest
    doctest.testmod()
