def is_prime(n):
    """
    >>> is_prime(2)
    True
    >>> is_prime(5)
    True
    >>> is_prime(3)
    True
    >>> is_prime(14)
    False
    >>> is_prime(1)
    False
    >>> is_prime(0)
    False
    >>> is_prime(1234)
    False
    >>> is_prime(199)
    True
    """
    count = n-1
    while count > 0:
        if n % count == 0:
            if count == 1:
                return True
            else:
                return False
        count -= 1
    if n == 1:
        return False
    if n == 0:
        return False
if __name__ == '__main__':
    import doctest
    doctest.testmod()
