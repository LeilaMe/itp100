def sum_of_squares_of_digits(n):
    """
    >>> sum_of_squares_of_digits(1)
    1
    >>> sum_of_squares_of_digits(9)
    81
    >>> sum_of_squares_of_digits(11)
    2
    >>> sum_of_squares_of_digits(121)
    6
    >>> sum_of_squares_of_digits(987)
    194
    """
    b = 0
    for a in str(n):
        a = int(a) * int(a)
        b += a
    return b

if __name__ == '__main__':
    import doctest
    doctest.testmod()
