def sum_of_squares_of_digits(n):
    b = 0
    for a in str(n):
        a = int(a) * int(a)
        b += a
    return b
A = int(input("write number: "))
print(sum_of_squares_of_digits(A))
