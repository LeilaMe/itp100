def base64decode(four_chars):
    """
    >>> base64decode('STOP')
    b'I3\x8f'
    >>> base64decode('VA==')
    b'T'
    >>> base64decode('/w==')
    b'\\xff'
    """
    digits = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'
    if len(four_chars) != 4 or not all([ch in digits + '=' for ch in four_chars]):
        raise ValueError(f'{four_chars} is not a value base64 encoded string')
#    ints = [digits.index(ch) for ch in four_chars]
    int1 = digits.index(four_chars[0])
    int2 = digits.index(four_chars[1])
#    b1 = (ints[0] << 2) | ((ints[1] & 48) >> 4)
    b1 = (int1 << 2) | ((int2 & 48) >> 4)
    if four_chars[2:] == '==':
        return bytes([b1])
    int3 = digits.index(four_chars[2])
    b2 = (int2 & 15) << 4 | int3 >> 2
#    b2 = (ints[1] & 15) << 4 | ints[2] >> 2
    if four_chars[3:] == '=':
        return bytes([b1, b2])
    int4 = digits.index(four_chars[3])
    b3 = (int3 & 3) << 6 | int4
#    b3 = ((ints[2] & 3) << 6) | ints[3]
    return bytes([b1, b2, b3])
if __name__=='__main__':
    import doctest
    doctest.testmod()
