def base64encode(three):
    """
    >>> base64encode(b'\\x5A\\x2B\\xE6')
    'Wivm'
    >>> base64encode(b'\\x49\\x33\\x8F')
    'STOP'
    >>> base64encode(b'\\xFF\\xFF\\xFF')
    '////'
    >>> base64encode(b'\\x00\\x00\\x00')
    'AAAA'
    >>> base64encode(b'Tst')
    'VHN0'
    >>> base64encode(b'T')
    'VA=='
    """
    digits = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'
    if len(three) < 1 or len(three) > 3:
        raise ValueError("Input should be 1 to 3 bytes")
#    b1, b2, b3 = three[0], three[1], three[2]
    b1 = three[0]
    index1 = b1 >> 2
    if len(three) == 1:
        index2 = (b1 & 3) << 4
        return f'{digits[index1]}{digits[index2]}=='
    b2 = three[1]
    index2 = (b1 & 3) << 4 | b2 >> 4
    if len(three) == 2:
        index3 = (b2 & 15) << 2
        return f'{digits[index1]}{digits[index2]}{digits[index3]}='
    b3 = three[2]
    index3 = (b2 & 15) << 2 | b3 >> 6
    index4 = b3 & 63
    return f'{digits[index1]}{digits[index2]}{digits[index3]}{digits[index4]}'

if __name__=='__main__':
    import doctest
    doctest.testmod()
