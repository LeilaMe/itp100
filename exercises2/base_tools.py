def to_binary(num):
    """
    Convert a decimal integer into a string representation of its binary (base2) digits.
    """ 
    new = ''
    if type(num) == int:
        if num == 0:
            return '0'
        while num > 0:
            a = num//2
            b = num % 2
            new = str(b) + new
            num = a
    else:
        return 'potato'
    return new

def to_base3(num):
    """
    Convert a decimal integer into a string representation of its base3 digits.
    """
    new = ''
    if type(num) == int:
        if num == 0:
            return '0'
        while num > 0:
            a = num//3
            b = num % 3
            new = str(b) + new
            num = a
    else:
        return 'potato'
    return new

def to_base4(num):
    """
    Convert a decimal integer into a string representation of its base4 digits.
    """
    new = ''
    if type(num) == int:
        if num == 0:
            return '0'
        while num > 0:
            a = num//4
            b = num % 4
            new = str(b) + new
            num = a
    else:
        return 'potato'
    return new

def to_base(num, base):
    """
    Convert a decimal integer into a string representation of the digits representing the number in the base (between 2 and 10) provided.
    """
    new = ''
    if type(num) == int:
        if num == 0:
            return '0'
        while num > 0:
            a = num//base
            b = num % base
            new = str(b) + new
            num = a
    else:
        return 'potato'
    return new

if __name__=='__main__':
    import doctest
    doctest.testmod()
