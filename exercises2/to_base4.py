def to_base4(num):
    """
    Convert a decimal integer into a string representation of its base4 digits.
    >>> to_base4(20)
    '110'
    >>> to_base4(28)
    '130'
    >>> to_base4(3)
    '3'
    >>> to_base4(4)
    '10'
    >>> to_base4(5)
    '11'
    >>> to_base4(0)
    '0'
    >>> to_base4(-5)
    ''
    >>> to_base4("dog")
    'potato'
    """ 
    new = ''
    if type(num) == int:
        if num == 0:
            return '0'
        while num > 0:
            a = num//4
            b = num % 4
            new = str(b) + new
            num = a
    else:
        return 'potato'
    return new

if __name__=='__main__':
    import doctest
    doctest.testmod()
