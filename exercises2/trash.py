def to_binary(num):
    """
    Convert a decimal integer into a string representation of its binary (base2) digits.
    >>> to_binary(10)
    '1010'
    >>> to_binary(12)
    '1100'
    >>> to_binary(1000)
    '1111101000'
    >>> to_binary(5)
    '101'
    >>> to_binary(0)
    '0'
    >>> to_binary(1)
    '1'
    >>> to_binary(2)
    '10'
    >>> to_binary(-5)
    'idk'
    >>> to_binary("dog")
    'potato'
    >>> to_binary(101010)
    '11000101010010010'
    """
    new = ''
    backwards = ''
    for digit in str(num):
        backwards = digit + backwards
    for dgt in backwards:
        if dgt == 0:
            continue
    if num == 1:
        return '1'
    if num == 0:
        return '0'
    if num > 1:
        
            if i//2 == 0:
                new = '0' + new
            else:
                new = '1' + new

    else:
        return 'idk'

if __name__=='__main__':
    import doctest
    doctest.testmod()
